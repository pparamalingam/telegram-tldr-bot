require 'telegram/bot'
require 'net/http/persistent'
require 'smmrize'
require 'mysql2'
require 'date'
require 'highscore'
require 'yaml'
require 'textrazor'
require 'aws-sdk'
require 'uri'
require 'byebug'
require 'rest-client'
require 'redd'

config = YAML.load_file("tldr-config.yaml")
token = config['telegram']['token']
client = Mysql2::Client.new(:host => config['db']['host'], :port => config['db']['port'], :username => config['db']['username'], :password => config['db']['password'], :database => config['db']['database'], :reconnect => true)
Smmrize::Defaults.api_key = config['smmry']['api_key']
tr_client = TextRazor::Client.new(config['textrazor']['api_key'], :classifiers => "textrazor_iab")
polly = Aws::Polly::Client.new(
  region: config['aws']['region'],
  access_key_id: config['aws']['access_id'],
  secret_access_key: config['aws']['secret_key']
)

begin
	Telegram::Bot::Client.run(token) do |bot|
	    last_article = Hash.new
		bot.listen do |message|
	    	begin
			  	case 
				    when message.text.include?("/help")
				    	bot.api.send_message(chat_id: message.chat.id, parse_mode: "html", text: 
				    		"Hello I'm the TLDR Bot. I'm here to summarize your chats!\n\n" \
				    		"<b>Commands</b>\n" \
				    		"/tldr <i>@user</i> - Displays context of user's messages for the day\n" \
							"/linktldr  - Summarize the content of the last posted URL\n " \
							"              If it is a Reddit link, it will show the top comment\n " \
				    		"/top   - Display the top 5 words of the day\n" \
							"/stats - Display total messages of the day and the most/least talkative\n" \
							"/combine <i>@user</i> - Combine user's messages for the day into one large message to catch up" \
				    		"/help  - Display this help screen\n")
				    #end when

				    when message.text.include?("/tldr")
				    	text = ""
				    	topics = ""
						category = ""						
				    	if message.entities.length > 1
							if message.entities[1].type == "text_mention"
								results = client.query("SELECT text FROM messages WHERE chat_id = #{message.chat.id} AND user_id = #{message.entities[1].user.id} AND DATE(FROM_UNIXTIME(date)) = curdate()")
								name = message.entities[1].user.first_name
							elsif message.entities[1].type == "mention"
								username = message.text.split('@')[1]
								results = client.query("SELECT text FROM messages WHERE chat_id = #{message.chat.id} AND username = '#{username}' AND DATE(FROM_UNIXTIME(date)) = curdate()")
								name_query = client.query("SELECT user_fn FROM messages WHERE chat_id = #{message.chat.id} AND username = '#{username}' LIMIT 1")
								name = name_query.first["user_fn"]
							end
				    		results.each do |row|
				    			text = text + row["text"] + "\n"
				    		end
				    		if text
				    			#sentiment = sentiment_service.analyze_document_sentiment(Google::Apis::LanguageV1beta1::AnalyzeSentimentRequest.update!(:content =>text, :type=>"PLAIN_TEXT"))
								response = tr_client.analyse(text)
								sentigem_response = RestClient.get 'https://api.sentigem.com/external/get-sentiment', {params: {'api-key': config['sentigem']['api_key'], 'text' => text}}
								if sentigem_response.code == 200
									sentiment = JSON.parse(sentigem_response.body)["polarity"]
								else
									sentiment = "N/A"
								end
				            	if response.topics && response.categories
				            		category = response.categories.first.label
				            		response.topics[0..4].each { |topic| topics = topics + topic.label + ", " }
				            		bot.api.send_message(chat_id: message.chat.id, parse_mode: "html", text: 
				            			"<b>Context of #{name}'s Messages</b>\n" \
				            			"Category - #{category}\n" \
				            			"Topics - #{topics.chomp(", ")}\n" \
										"Overall Sentiment - #{sentiment}")
				            	else
				            		bot.api.send_message(chat_id: message.chat.id, parse_mode: "html", text: "<b>Not enough text to analyse!</b>") 
				            	end
				        	end
				    	end
				    #end when

				   	when  message.text.include?("http")
							begin
								uri = URI.extract(message.text).select {|str| str.include?('http')}.first
								url = URI.parse(uri)
								if (url.host.include?("reddit"))
									reddit = Redd.it(
										user_agent: 'Redd:MyFancyBot:v1.2.3',
										client_id: config['reddit']['client_id'],
										secret: config['reddit']['secret']
									)
									post_id = url.path.split("/")[4]
									top_comment = reddit.from_ids("t3_" + post_id).first.comments.first.body
									top_comment_score = reddit.from_ids("t3_" + post_id).first.comments.first.score
									last_article[message.chat.id] = { :top_comment => top_comment.split("[").first, :top_comment_score => top_comment_score }
								else
				   					uri = URI.extract(message.text).select {|str| str.include?('http')}.first
									last_article[message.chat.id] = Smmrize.webpage(url: uri , length: 4, with_break: true)
								end
							rescue
								last_article[message.chat.id] = nil
								next
							end
					#end when	

				   	when message.text.include?("/linktldr")
				        if last_article[message.chat.id] && last_article[message.chat.id]["sm_api_content"]
					  		puts last_article[message.chat.id]["sm_api_content"]
				          	bot.api.send_message(chat_id: message.chat.id, parse_mode: "html", text: "<b>"+last_article[message.chat.id]["sm_api_title"]+"</b>\n\n"  + last_article[message.chat.id]["sm_api_content"].to_s.gsub("[BREAK]", "\n")) 
							polly_resp = polly.synthesize_speech({
							  engine: "standard",
							  output_format: "mp3", # required, accepts mp3, ogg_vorbis, pcm
							  text: last_article[message.chat.id]["sm_api_content"].to_s.gsub("[BREAK]", "\n"), # required
							  text_type: "text", # accepts ssml, text
							  voice_id: "Brian", # required, accepts Geraint, Gwyneth, Mads, Naja, Hans, Marlene, Nicole, Russell, Amy, Brian, Emma, Raveena, Ivy, Joanna, Joey, Justin, Kendra, Kimberly, Salli, Conchita, Enrique, Miguel, Penelope, Chantal, Celine, Mathieu, Dora, Karl, Carla, Giorgio, Mizuki, Liv, Lotte, Ruben, Ewa, Jacek, Jan, Maja, Ricardo, Vitoria, Cristiano, Ines, Carmen, Maxim, Tatyana, Astrid, Filiz
							})
							bot.api.send_audio(chat_id: message.chat.id, audio: Faraday::UploadIO.new(polly_resp.audio_stream, polly_resp.content_type, "tldr-audio.mp3"))
						elsif last_article[message.chat.id] && last_article[message.chat.id][:top_comment]
							puts last_article[message.chat.id]	
							bot.api.send_message(chat_id: message.chat.id, parse_mode: "html", text: "<b> Top Reddit Comment </b>\n\n" + last_article[message.chat.id][:top_comment]+"\n\n" + "<b> Score </b>\n\n" + last_article[message.chat.id][:top_comment_score].to_s) 
						else 
					  		bot.api.send_message(chat_id: message.chat.id, text: "I couldn't find the last link posted, please repost the link.")
						end
					#end when

				   	when message.text.include?("/top")
						keywords = ""
						output = ""
						results = client.query("SELECT text FROM messages WHERE chat_id = #{message.chat.id} AND DATE(FROM_UNIXTIME(date)) = curdate()")
						results.each do |row|
				        	keywords = keywords + row["text"] + " "
						end
					
						blacklist = Highscore::Blacklist.load_file "stopwords.txt"
						ranking  = Highscore::Content.new keywords, blacklist
						ranking.configure do
					 		set :ignore_case, true
					 		set :stemming, false
							set :short_words_threshold, 3
						end
						ranking.keywords.top(5).each do |k|
					 		output = output +  "#{k.text}, weight: #{k.weight}\n" 
						end
						bot.api.send_message(chat_id: message.chat.id, parse_mode: "html", text: "<b>Top 5 words of the day</b>\n\n" + "#{output}")
					#end when
						
				  	when message.text.include?("/stats")
						top_user = ""
						bottom_user = ""
						total_msgs = ""

						results_top = client.query("SELECT user_fn, COUNT(*) AS count FROM messages WHERE chat_id = #{message.chat.id} AND DATE(FROM_UNIXTIME(date)) = curdate() GROUP BY user_fn ORDER BY count DESC LIMIT 1;")
						results_bottom = client.query("SELECT user_fn, COUNT(*) AS count FROM messages WHERE chat_id = #{message.chat.id} AND DATE(FROM_UNIXTIME(date)) = curdate() GROUP BY user_fn ORDER BY count ASC LIMIT 1;")
						results_total = client.query("SELECT message_id, COUNT(*) AS count FROM messages WHERE chat_id = #{message.chat.id} AND DATE(FROM_UNIXTIME(date)) = curdate();")

						results_top.each do |row|
					 		top_user = row["user_fn"]
						end
						results_bottom.each do |row|
					 		bottom_user = row["user_fn"]
						end		
						results_total.each do |row|
					 		total_msgs = row["count"]
						end

						bot.api.send_message(chat_id: message.chat.id, parse_mode: "html", text: 
							"<b>Stats of the Day</b>\n\n" \
							"<b>Biggest Mouth:</b>\n" + "#{top_user}\n" \
							"<b>Shy One:</b>\n" + "#{bottom_user}\n" \
							"<b>Total Messages:</b>\n" + "#{total_msgs}\n"
						)
					#end when

					when message.text.include?("/combine")
						text = ""
						if message.entities.length > 1
							if message.entities[1].type == "text_mention"
								results = client.query("SELECT text FROM messages WHERE chat_id = #{message.chat.id} AND user_id = #{message.entities[1].user.id} AND DATE(FROM_UNIXTIME(date)) = curdate()")
								name = message.entities[1].user.first_name
							elsif message.entities[1].type == "mention"
								username = message.text.split('@')[1]
								results = client.query("SELECT text FROM messages WHERE chat_id = #{message.chat.id} AND username = '#{username}' AND DATE(FROM_UNIXTIME(date)) = curdate()")
								name_query = client.query("SELECT user_fn FROM messages WHERE chat_id = #{message.chat.id} AND username = '#{username}' LIMIT 1")
								name = name_query.first["user_fn"]
							end
							results.each do |row|
								text = text + row["text"] + "\n\n"
							end
							if text
								bot.api.send_message(chat_id: message.chat.id, parse_mode: "html", text: 
									"<b>List of #{name}'s Messages</b>\n" \
									"#{text}"
								)
				        	end
				    	end
					#end when

			  	end #end case

			 	puts message.inspect
			 	if !message.text.nil? && message.text !~ /^\// && message.text !~ /^(http|https):\/\//
			 		client.query("INSERT INTO messages (message_id, user_id, chat_id, date, text, user_fn, user_ln, username) VALUES ('#{message.message_id}', '#{message.from.id}', '#{message.chat.id}', '#{message.date}', '#{client.escape(message.text)}', '#{message.from.first_name}', '#{message.from.last_name}', '#{message.from.username}')")
			 	end
			# rescue SignalException, Interrupt => e
			# 	puts e
			# 	bot.api.send_message(chat_id: message.chat.id, parse_mode: "html", text: 
			# 		"My master has shut me down! :(\n\n")				 
		 	rescue Exception => e
				token = config['telegram']['token']
				#client = Mysql2::Client.new(:host => "localhost", :username => config['db']['username'], :password => config['db']['password'], :database => config['db']['database'], :reconnect => true)
				Smmrize::Defaults.api_key = config['smmry']['api_key']
				tr_client = TextRazor::Client.new(config['textrazor']['api_key'], :classifiers => "textrazor_iab")	
				polly = Aws::Polly::Client.new(
				  region: config['aws']['region'],
				  access_key_id: config['aws']['access_id'],
				  secret_access_key: config['aws']['secret_key']
				)	
				next	 		
		 	end

	    end #end bot listen loop
	end #end client
# rescue SignalException, Interrupt => e
# 	puts e
rescue
	retry
end
