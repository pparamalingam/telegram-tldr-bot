CREATE TABLE `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message_id` bigint(16) DEFAULT NULL,
  `user_id` bigint(16) DEFAULT NULL,
  `chat_id` bigint(16) DEFAULT NULL,
  `date` int(11) DEFAULT NULL,
  `text` mediumtext,
  `user_fn` varchar(255) DEFAULT NULL,
  `user_ln` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9424 DEFAULT CHARSET=utf8;