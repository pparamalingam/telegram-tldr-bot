# Telegram TLDR Bot

A Telegram bot written in Ruby that provides a summary of conversations and posted links.
The bot also provides statistics of conversations.  

Bot is powered by telegram-bot-ruby (https://github.com/atipugin/telegram-bot-ruby)  

Summary is powered by SMMRY (http://smmry.com/)  

Keywords are powered by Highscore (https://github.com/domnikl/highscore)  

## Dependencies

1. mysql
2. Token from Telegram Bot API
3. API key from SMMRY API
4. API key from TextRazor API
5. AWS key/secrety for Amazon Polly API

## Installation

1. Pull source: `git clone https://gitlab.com/pparamalingam/telegram-tldr-bot.git && cd telegram-tldr-bot/`
2. Install gem dependencies: `bundle install`
3. Setup config in tldr-config.yaml: `vim tldr-config.yaml`
4. Run `tldr-bot_setup.mysql` located in `setup/` within your database 

MySQL Table Script

``` sql
CREATE TABLE `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message_id` bigint(16) DEFAULT NULL,
  `user_id` bigint(16) DEFAULT NULL,
  `chat_id` bigint(16) DEFAULT NULL,
  `date` int(11) DEFAULT NULL,
  `text` mediumtext,
  `user_fn` varchar(255) DEFAULT NULL,
  `user_ln` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9424 DEFAULT CHARSET=utf8;
```

YAML Template
``` ruby
db:
  host: "host"
  username: "username"
  password: "password"
  database: "database"

telegram:
  token: "token"

smmry:
  api_key: "api_key"

textrazor: 
  api_key: "api_key"

aws:
  region: "us-east-2"
  access_id: "access_id"
  secret_key: "secret_key"
```

## Usage

Run application: `ruby tldr.rb`

Add bot to Telegram conversation and explore various commands

## Notes

API features that are unwanted can be disabled by removing the command block of said API.

## Known Issues

SMMRY API gem had problems for me at first, had to edit some of the requires from uppercase to lowercase.

## Contributing

1. Fork it!
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request :D

## History

TODO: Write history

## Credits

TODO: Write credits

## License