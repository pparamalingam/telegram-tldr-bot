FROM ruby:2.7


WORKDIR /Users/preshoth/projects/telegram-tldr-bot

RUN gem install bundler --version "2.1.4"

COPY Gemfile Gemfile.lock ./
RUN bundle install

COPY . .

CMD bundle exec ruby tldr.rb